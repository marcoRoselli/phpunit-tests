<?php

include ("calculator.php");

class calculatorTest extends PHPUnit_Framework_TestCase 
{

	function setup () {
		$this->calc = new Calculator();
	}
	/**
		* @dataProvider additionProviderSum
  */
	public function testAddo($val1,$val2,$expectedRes) {
		$result = $this->calc->add($val1,$val2);
		$this->assertEquals($result,$expectedRes);
	}	

	public function additionProviderSum()
    {
        return array(
          array(0,0,0),
          array(0,1,0),
          array(1,0,0),
          array(1,1,1)
        );
    }
	
}
